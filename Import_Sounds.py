# -*- coding: utf-8 -*-
import os
import vs
import sfmApp
from filesystem import valve
import wave
import contextlib
from PySide import QtGui


movie = sfmApp.GetMovie()


def convert_to_valve_path(path):
    """
    Transform an absolute path into a Valve path
    A Valve path is a filepath that looks like 'models/puxtril/dojo/wall.mdl'
    """
    game_path = os.path.normpath(valve.game())
    if not path.startswith(game_path):
        if not ask_to_continue(path):
            return None
    valve_path = path.replace(game_path, '', 1)
    valve_path = '\\'.join(valve_path.lstrip('\\').split('\\')[2:])
    return str(valve_path)


def ask_to_continue(model_name):
    msg_box = QtGui.QMessageBox()
    msg_box.setWindowTitle('Model Import Error')
    msg_box.setText('{} is not in the SFM directory, will not import.'.format(model_name))
    ok_btn = msg_box.addButton('OK', QtGui.QMessageBox.ActionRole)
    ignore_btn = msg_box.addButton('Ignore', QtGui.QMessageBox.ActionRole)
    msg_box.setDefaultButton(ok_btn)
    msg_box.exec_()
    if msg_box.clickedButton() == ignore_btn:
        return True
    return False


def get_sound_duration(sound_path):
    with contextlib.closing(wave.open(sound_path, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        sound_duration = frames / float(rate)
    return sound_duration


def import_sounds(channel):
    channels = ['Dialog', 'Music', 'Effects']
    channel_index = channels.index(channel)

    sounds, ext_filter = QtGui.QFileDialog.getOpenFileNames(caption='Import WAV sound(s) to {}'.format(channel),
                                                            dir=valve.game(),
                                                            filter='(*.wav)')

    # if any sounds have been selected, proceed. otherwise, do nothing
    if sounds:
        for sound in sounds:
            duration = get_sound_duration(sound)

            sound_name = convert_to_valve_path(sound)
            # if the sound was from an incorrect directory - do nothing. otherwise, proceed.
            if sound_name:
                sound_basename_no_ext, ext = [str(x) for x in os.path.splitext(os.path.basename(sound_name))]

                audio_elem = vs.CreateElement('DmeGameSound', sound_name, movie.GetFileId())
                # setting the sound name. the '#' is necessary to -
                # disable ambient "reverb" map's effects applied to all sounds by default
                audio_elem.soundname = '#' + sound_name
                audio_elem.volume = 1
                audio_elem.pitch = 100
                # audio_elem.static = True # todo: find out what this is for
                audio_clip = vs.CastElementAsDmeGameSound(audio_elem)

                sound_elem = vs.CreateElement('DmeSoundClip', sound_basename_no_ext, movie.GetFileId())
                sound_elem.sound = audio_clip
                sound_elem.timeFrame.duration = duration
                sound_clip = vs.CastElementAsDmeSoundClip(sound_elem)
                # Enabling the sound wave display for users' convenience sake
                sound_clip.showwave = True

                movie.trackGroups[0].tracks[channel_index].AddClip(sound_clip)


def choose_channel():
    msg_box = QtGui.QMessageBox()
    msg_box.setWindowTitle('Choose your channel')
    msg_box.setText('<p align="center">Choose the channel to import your sounds to:</p>')
    dialog_btn = msg_box.addButton('Dialog', QtGui.QMessageBox.ActionRole)
    music_btn = msg_box.addButton('Music', QtGui.QMessageBox.ActionRole)
    effects_btn = msg_box.addButton('Effects', QtGui.QMessageBox.ActionRole)
    msg_box.addButton('Cancel', QtGui.QMessageBox.ActionRole)
    msg_box.setDefaultButton(dialog_btn)
    msg_box.exec_()
    if msg_box.clickedButton() == dialog_btn:
        import_sounds('Dialog')
    elif msg_box.clickedButton() == music_btn:
        import_sounds('Music')
    elif msg_box.clickedButton() == effects_btn:
        import_sounds('Effects')
    msg_box.close()


if __name__ == '__main__':
    choose_channel()
