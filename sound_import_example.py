import vs
import sfmApp

movie = sfmApp.GetMovie()

audioElem = vs.CreateElement("DmeGameSound", r"music\bonus_round_30.wav", movie.GetFileId())
audioElem.soundname = r"music\bonus_round_30.wav"
audioElem.volume = 1
audioElem.pitch = 100
audioElem.static = True
audioClip = vs.CastElementAsDmeGameSound(audioElem)

soundElem = vs.CreateElement("DmeSoundClip", "TestSound", movie.GetFileId())
soundElem.sound = audioClip
soundElem.timeFrame.duration = 3.7 # Should be dynamic
soundClip = vs.CastElementAsDmeSoundClip(soundElem)

movie.trackGroups[0].tracks[0].AddClip(soundClip)
